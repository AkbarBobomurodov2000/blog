interface IAlbum{
    id:number;
    userId:number;
    title:string;
}

export default IAlbum;