import IPostInterface from './post';

type PostsType=Array<IPostInterface>;

export default PostsType;