import IComment from './comment';

type IComments=Array<IComment>;

export default IComments;