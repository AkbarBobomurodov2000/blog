import { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import Button from 'src/components/button'
// import styles from "./home.module.scss";
import './home.module.scss'
import { Transition, CSSTransition } from 'react-transition-group';


const Home = () => {

    const [isAppear, setIsAppear] = useState<boolean>(false);

    useEffect(() => {
        setIsAppear(true);
    }, []);

  

    return (

        <div className='section'>
            <CSSTransition timeout={2000} in={isAppear} classNames='welcome'>
                <div className='welcome'>
                    <Row>
                        <Col xl="6">
                            <h1 className='white'>This is Blog site in NextJS</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col xl="6">
                            <p className='white'>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet dolor
                                praesentium, architecto assumenda iusto repellat similique.
                                Doloribus illum modi eius eligendi earum at atque officia, alias
                                fugit? Dicta, placeat nostrum.
          </p>
                        </Col>
                    </Row>
                    <Row>
                        <Button outline={true} text='Read More'/>
                    </Row>
                </div>
            </CSSTransition>
        </div>
    );
};

export default Home;
