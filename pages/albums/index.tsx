import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { Card, CardBody, CardTitle, Container, Row, Col } from "reactstrap";

import IAlbum from "types/album";
import IAlbums from "types/albums";
import 'bootstrap/dist/css/bootstrap.min.css';
export default function Albums(
    props: InferGetServerSidePropsType<typeof getServerSideProps>
) {
    return (
        <Container fluid={true}>

            <Row>
                {props.albums.map((album: IAlbum, index: number) => (
                    <Col  lg={3} md={4}>
                        <Card>
                            <CardBody>
                                <CardTitle>{album.title}</CardTitle>
                            </CardBody>
                        </Card>
                    </Col>
                ))}
            </Row>


        </Container>
    );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const res = await fetch(`${process.env.BASE_URL}/albums`);
    const albums: IAlbums = await res.json();

    return {
        props: {
            albums,
        },
    };
};
