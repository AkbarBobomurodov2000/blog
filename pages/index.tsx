import Head from 'next/head'
import Button from 'src/components/button';
import Navbar from 'src/components/navbar';
import { Container } from 'reactstrap'
import { default as Content } from 'src/components/home'

import 'styles/Home.module.scss';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Home() {


  console.log('test', process.env.BASE_URL, process.env.baseUrl)

  return (
    <Container fluid={true} className='custom-container'>
      <header className='header'>
        <Navbar/>
      </header>
      <Content />
    </Container>
  )
}


